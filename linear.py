# Linear search algorithm

def linearSearch(ls, value):
    #returns index of the found value or -1 if not found
    for i in range(len(ls)):
        if ls[i] == value:
            return i
    return -1



data  = [1,2,3,4,5,6,7,8,9,10]

idx = linearSearch(data, 7)

print(idx)