#!/anaconda3/bin/python

numbers = [1, 2, 3, 4]
squares = [n**2+n for n in numbers]

print(squares)  # Output: [1, 4, 9, 16]