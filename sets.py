#!/anaconda3/bin/python

#Set Math Operations
# 1. intersection
# 2. union
# 3. difference
# 4. sysmetric difference

SetA = set(['Red', 'Blue', 'Green', 'Black'])
SetB = set(['Black', 'Green', 'Yellow', 'Orange'])
SetX = SetA.union(SetB)
SetY = SetA.intersection(SetB)
SetZ = SetA.difference(SetB)
SetN = SetB.difference(SetA)


print('Union:')
print(SetX)
print('Intersection: ')
print(SetY)
print('Difference: ')
print(SetZ)
print('Difference(2): ')
print(SetN)

print('Formatted Lists: ')
print('SetX: {0}\nSetY: {1}\nSetZ: {2}'.format(SetX,SetY,SetZ))