import os

os.chdir('c:/data')

for f in os.listdir():
    f_name, f_ext = os.path.splitext(f)
    f_type, f_num = f_name.split('_')
    
    #Strip # and pad number with 0 using zfill
    f_num = f_num[1:].zfill(2)
   
    new_name = '{}-{}{}'.format(f_num, f_type, f_ext)

    os.rename(f, new_name)