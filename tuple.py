#!/anaconda3/bin/python

tplData = (1,2,3, (4,5,6, (7,8,9)))

for val1 in tplData:
	if type(val1) == int:
		print(val1)
	else:
		for val2 in val1:
			if type(val2) == int:
				print ("\t", val2)
			else:
				for val3 in val2:
					print("\t\t", val3)