"""
Binary Search

Assuming a sorted list, start the search in the middle; 
if the middle value is greater than the search item, search the lower half of the remaining range
if lesser than the search term, search in the upper half;
repeat until the search item is found or the list exhausted

"""


data = [-7, 52, 55, 90, 98, 178, 203, 340, 477, 1002, 1881]

def binarySearch(ls, value):
    #return index of the found value or -1 if not found
    startIdx = 0
    endIdx = len(ls)-1

    while startIdx <= endIdx:
        midIdx = startIdx + int((endIdx - startIdx) / 2)
        midVal = ls[midIdx]
        if midVal == value:
            return midIdx
        elif midVal < value:
            startIdx = midIdx + 1
        elif midVal > value:
            startIdx = midIdx -1
    return -1


idx = binarySearch(data, 203)
print(idx)
