#!/anaconda3/bin/python

dictColors = {'Orange':1, 'Blue':2, 'Pink':3}

print(dictColors['Orange'])
print(dictColors.keys())
print(dictColors.values())